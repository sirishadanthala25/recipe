
import './App.css';
import Navbar from './components/Navbar';
import Home from './pages/home';
import Recipe from './pages/Recipes';
import {BrowserRouter as Router,Routes,Route} from 'react-router-dom';
import RecipeDetails from './components/RecipeDetails';
// import Offers from './pages/offers';
import Login from './components/login';


import 'bootstrap/dist/css/bootstrap.min.css'
function App() {
  return (
  <Router>
    
    <Navbar/>
    <Routes>
    <Route path="/" element={<Home/>}/>
    <Route path="/RecipeS" element={<Recipe/>}/>
    {/* <Route path="/offers" element={<Offers/>}/> */}
    <Route path='/login' element={<Login/>}/>
    <Route path='/recipes/:recipeId' element={<RecipeDetails/>}/>
    </Routes>
    </Router>
    
  );
}

export default App;
