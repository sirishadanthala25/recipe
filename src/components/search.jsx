// import React from 'react';
import { useState } from 'react';
import { Grid,Form,Input } from 'semantic-ui-react';
const search = ({setSearchedQuery}) => {
    const [value,setValue]=useState("");
    const onFormSubmit=()=>{
        setSearchedQuery(value);
    }
  return (
    <Grid column={2} textAlign='center' className='search-box'>
        <Grid.Column>
            <h2 className='search-heading'>Search Recipes with <span style={{color:"red"}}> Our recipes</span></h2>
            <h4>Input Recipes Seperated by comma</h4>
            <Form onSubmit={onFormSubmit}>
            <Input
            placeholder='Tomato,potato,pizza'
           action={{icon:'search',color:'green'}}
            onChange={(e)=>setValue(e.target.value)}
            value={value}
            />
            </Form>
        </Grid.Column>
    </Grid>
  );
}

export default search;
