import React from 'react';
import {Menu} from 'semantic-ui-react';
// import { logo } from '../constant/constant';
import { Link } from 'react-router-dom';
const Navbar = () => {
  return (
    <div className='navbar'>
    <Menu borderless fixed='top' >
      <img src='logo.jpg' alt='logo' style={{height:60}}/>
      <Menu.Item name="home" as={Link} to="/"/>
       <Menu.Item name="Recipes" as={Link} to="/recipes"/> 
      {/* <Menu.Item name="offers" as={Link} to="/offers"/>  */}
      
      
    </Menu>
    </div>
  );
}

export default Navbar;
