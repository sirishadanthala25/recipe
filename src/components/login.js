





// import React, { useState } from 'react';

// import axios from 'axios';




// function RegistrationForm() {
// const [name, setName] = useState('');
// const [lastname,setLastname]=useState('');
// const [salary, setSalary] = useState('');
// const [email, setEmail] = useState('');
// const [password, setPassword] = useState('');



// const handleRegister = async (event) => {
// event.preventDefault();
// setName("");
// setLastname("");
// setSalary("");
// setEmail("");
// setPassword("");
// try {
// const response = await axios.post('http://localhost:7005/regester', {
// name,
// lastname,
// salary,
// email,
// password
// });


// if (response.data.message === '1 document inserted') {
// alert('Registration successful');
// } else {
// alert('Registration failed');
// }
// } catch (error) {
// console.error(error);
// }
// };


// return (
//     <div id='registration' >

   
// <div className="registration-form">
//    <center> <h1><u>REGESTRATION FORM</u></h1></center>
// <form onSubmit={handleRegister}>
// <div className="form-group">
// <label htmlFor=" First name"> First Name:</label>
// <input className='form1'
// type="text"
// id="name"
// value={name}
// placeholder='enter your first name'
// onChange={(e) => setName(e.target.value)}
// required
// style={{marginLeft:75}}
// /> 
// </div> <br></br>
// <div className='form-group'>
// <label htmlFor='lastname'>Last name:</label>
// <input className='form1'
// type="text"
// id="name"
// placeholder='enter your lastname'
// value={name}
// onChange={(e) => setName(e.target.value)}
// required
// style={{marginLeft:75}}
//  /> </div> <br></br>
// <div className='form-group'>
// <label htmlFor='email'>Email:</label>
// <input className='form1'
// type="email"
// id="email"
// placeholder='enter your Email'
// value={email}
// onChange={(e) => setEmail(e.target.value)}
// required
// style={{marginLeft:75}}
// />

//     </div><br></br> 
//     <div className="form-group">
// <label htmlFor=" password"> Password:</label>
// <input className='form1'
// type="password"
// id="password"
// value={password}
// placeholder='enter your password'
// onChange={(e) => setPassword(e.target.value)}
// required
// style={{marginLeft:75}}
// /> 
// </div> <br></br>

// <div id='btn'>

// <button type="submit" className="btn btn-primary">
// Submit

// </button>
// </div>
// </form>
// </div>
// </div>


// )
// }
// export default RegistrationForm;







import React, { useState } from 'react';
import axios from 'axios';
import { Form, Input, Button, Header } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

function RegistrationForm() {
  const [name, setName] = useState('');
  const [lastname, setLastname] = useState('');
  const [salary, setSalary] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleRegister = async (event) => {
    event.preventDefault();
    setName('');
    setLastname('');
    setSalary('');
    setEmail('');
    setPassword('');
    try {
      const response = await axios.post('http://localhost:5000/register', {
        name,
        lastname,
        email,
        password,
      });

      if (response.data.message === '1 document inserted') {
        alert('Registration successful');
      } else {
        alert('Registration failed');
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div id="registration">
      <div className="registration-form">
        <center>
          <Header as="h1" icon>
            REGISTRATION FORM
          </Header>
        </center>
        <Form onSubmit={handleRegister}>
          <Form.Field>
            <label htmlFor="name">First Name:</label>
            <Input
              type="text"
              id="name"
              value={name}
              placeholder="Enter your first name"
              onChange={(e) => setName(e.target.value)}
              required
            />
          </Form.Field>
          <Form.Field>
            <label htmlFor="lastname">Last Name:</label>
            <Input
              type="text"
              id="lastname"
              placeholder="Enter your last name"
              value={lastname}
              onChange={(e) => setLastname(e.target.value)}
              required
            />
          </Form.Field>
          <Form.Field>
            <label htmlFor="email">Email:</label>
            <Input
              type="email"
              id="email"
              placeholder="Enter your email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Field>
          <Form.Field>
            <label htmlFor="password">Password:</label>
            <Input
              type="password"
              id="password"
              value={password}
              placeholder="Enter your password"
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Field>
          <Button type="submit" primary  as={Link}
            to="/Recipes">
            Submit
            
          </Button>
        </Form>
      </div>
    </div>
  );
}

export default RegistrationForm;
