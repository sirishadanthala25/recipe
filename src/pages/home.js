import React from 'react';
import Header from '../components/header';
import { Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const home = () => {
  return (
    <Header title="Our Recipes" bgClass="bg-image">
      

<Button
     content="REGESTER FOR SEARCH RECIPES"
     color='primary'
     as={Link}
     to="/login"
     size='big'
    />
    </Header>
  )
}
export default home;
